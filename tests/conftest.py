"""
Pytest fixtures.

The scope module runs the fixture per module.

https://betterprogramming.pub/understand-5-scopes-of-pytest-fixtures-1b607b5c19ed
"""

import inspect
import json
import os
import pytest
import yaml
from importlib.metadata import version
from pathlib import Path

SNYK_ORG = 'wols_disabled'  # https://github.com/snyk/snyk-python-plugin/issues/214
SNYK_USER = os.environ.get('USER', 'nobody')
VERSION_FILE = os.path.join('src', 'tecdocgen', 'VERSION')
VERSIONS_FILE = '.versions.json'


def __print_f_name():
    """Print the name of calling fixture in tests."""
    print('DEBUG', 'use fixture', inspect.stack()[1][3])


@pytest.fixture(scope='module')
def f_adoc():
    """
    Add a string attribute.

    File name.
    """
    pytest.f_adoc = 'docs/modules/ROOT/pages/imprint.adoc'
    pytest.f_html = 'docs/modules/ROOT/pages/imprint.html'
    __print_f_name()


@pytest.fixture(scope='module')
def f_antora_module():
    """
    Add four pytest attributes.

    f_antora_module : str
    f_antora_module_diagrams : str
    f_antora_module_diagrams_list : list
        all file names in f_antora_module_diagrams
    f_antora_module_diagrams_file : str
        a file name from f_antora_module_diagrams_list
    """
    pytest.f_antora_module = 'docs/modules/ROOT'
    pytest.f_antora_module_diagrams = pytest.f_antora_module + '/' + '_diagrams'
    pytest.f_antora_module_diagrams_list = []
    for __diagram in Path(pytest.f_antora_module_diagrams).iterdir():
        pytest.f_antora_module_diagrams_list.append(__diagram.name)
    pytest.f_antora_module_diagrams_file = 'overview.adoc'
    __print_f_name()


@pytest.fixture(scope='module')
def f_antora_playbook():
    """
    Add three pytest attributes.

    f_antora_playbook : str
        docs/public.yml
    f_antora_playbook_all : dict
        key, values from docs/public.yml
    f_antora_playbook_dump
        json.dumps from f_antora_playbook_all
    """
    pytest.f_antora_playbook = 'docs/preview.yml'
    with open(pytest.f_antora_playbook, mode='rt', encoding='utf-8') as __f:
        pytest.f_antora_playbook_all  = yaml.safe_load(__f)
        pytest.f_antora_playbook_dump = json.dumps(pytest.f_antora_playbook_all)
    __print_f_name()


@pytest.fixture(scope='module')
def f_antora_url():
    """Add pytest attribute."""
    pytest.f_antora_url = '.'
    __print_f_name()


@pytest.fixture(scope='module')
def f_antora_url_branch():
    """Add pytest attribute."""
    pytest.f_antora_url_branch = 'develop'
    __print_f_name()


@pytest.fixture(scope='module')
def f_antora_url_branches():
    """Add pytest attribute."""
    pytest.f_antora_url_branches = ['main']
    __print_f_name()


@pytest.fixture(scope='module')
def f_docgen_state():
    """Add pytest attribute."""
    pytest.f_docgen_state = {
        10: {
            'id': 'instanced',
            'allowed_events': [
                'cancel',
                'generate',
            ],
        },
        60: {
            'id': 'generated',
            'allowed_events': [
                'cancel',
                'finish',
            ],
        },
        70: {
            'id': 'finished',
            'allowed_events': [
                'clean',
            ],
        },
        80: {
            'id': 'canceled',
            'allowed_events': [
                'clean',
            ],
        },
        90: {
            'id': 'cleaned',
            'allowed_events': [],
        },
    }
    __print_f_name()


@pytest.fixture(scope='module')
def f_snyk():
    """Add pytest attribute."""
    pytest.f_snyk = {
        'command': [
            'snyk',
            'test',
            '--file=requirements-dev.txt',
            '--package-manager=pip',
        ],
        'file': 'requirements-dev.txt',
    }
    __print_f_name()
    if SNYK_ORG != SNYK_USER:
        pytest.skip('Snyk test is not enabled.')


@pytest.fixture(scope='module')
def f_tecdocgen_conf_file():
    """Add pytest attribute."""
    pytest.f_tecdocgen_conf_file = 'docs/tecdocgen.yml'
    __print_f_name()


@pytest.fixture(scope='module')
def f_tecdocgen_config():
    """Add pytest attribute."""
    pytest.f_tecdocgen_config = {
        'antora': {
            'playbook': './public.yml',
        },
        'tecdocgen': {
            'attachments': [
                {
                    'pdf': 'tecdocgen-development.pdf',
                    'adoc': './modules/ROOT/pages/development.adoc',
                },
                {
                    'pdf': 'development.pdf',
                    'adoc': './modules/ROOT/pages/development.adoc',
                },
            ],
        },
    }
    __print_f_name()


@pytest.fixture(scope='module')
def f_util_versions():
    """Add pytest attribute."""
    with open(os.path.join(VERSIONS_FILE)) as __f:
        pytest.f_util_versions = json.load(__f)
    __print_f_name()


@pytest.fixture(scope='module')
def f_version():
    """Return the package version."""
    with open(os.path.join(VERSION_FILE)) as __f:
        pytest.f_version = __f.read().strip()
    __print_f_name()
