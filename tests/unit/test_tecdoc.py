import pytest
import unittest
from tecdocgen import tecdoc


@pytest.mark.usefixtures('f_tecdocgen_conf_file', 'f_tecdocgen_config')
class TestTecDocConfig(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._config = tecdoc.TecDocConfig(pytest.f_tecdocgen_conf_file)

    def test_0_class(self) -> None:
        self.assertIsInstance(self._config, tecdoc.TecDocConfig)

    def test_1_attribute(self) -> None:
        for __attr in ['all', 'antora', 'file', 'tecdocgen']:
            with self.subTest(__attr=__attr):
                self.assertTrue(hasattr(self._config, __attr))

    def test_2_file(self) -> None:
        self.assertEqual(self._config.file, pytest.f_tecdocgen_conf_file)
        print('DEBUG', self._config.file)

    def test_3_antora(self) -> None:
        self.assertEqual(
            self._config.antora,
            pytest.f_tecdocgen_config['antora'],
        )
        print('DEBUG', self._config.antora)

    def test_4_tecdocgen(self) -> None:
        self.assertEqual(
            self._config.tecdocgen,
            pytest.f_tecdocgen_config['tecdocgen'],
        )
        print('DEBUG', self._config.tecdocgen)

    def test_5_all(self) -> None:
        self.assertEqual(
            self._config.all,
            pytest.f_tecdocgen_config,
        )
        print('DEBUG', self._config.all)

    @pytest.mark.dependency()
    def test_9_pass(self) -> None:
        pass


class TestTecDoc(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._tecdoc = tecdoc.TecDoc(pytest.f_tecdocgen_conf_file)

    def test_0_class(self) -> None:
        self.assertIsInstance(self._tecdoc, tecdoc.TecDoc)

    @pytest.mark.dependency(
        depends=['TestTecDocConfig::test_9_pass'],
        scope='module',
    )
    def test_1_attribute(self) -> None:
        self.assertTrue(hasattr(self._tecdoc, 'config'))

    def test_3_config(self) -> None:
        self.assertIsInstance(self._tecdoc.config, tecdoc.TecDocConfig)

    @pytest.mark.dependency(
        depends=['tests/unit/test_docgen.py::TestGenerator::test_9_pass'],
        scope='session',
    )
    def test_5_generate(self) -> None:
        self.assertTrue('generate' in dir(self._tecdoc))
        self.assertIsNone(self._tecdoc.generate())
