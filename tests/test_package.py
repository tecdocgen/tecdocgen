import pytest
import tecdocgen
import unittest


@pytest.mark.usefixtures('f_version')
class TestModule(unittest.TestCase):
    def test_0_version(self) -> None:
        self.assertEqual(tecdocgen.__version__, pytest.f_version)
        print('DEBUG', tecdocgen.__version__)
