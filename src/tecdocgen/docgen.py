"""
TecDocGen docgen module.

Classes
-------
GenState
Generator
"""

import logging
from statemachine import StateMachine, State

logger = logging.getLogger(__name__)


class Generator():
    """
    Generator object.

    Attributes
    ----------
    state : object
    """

    def __init__(self) -> None:
        """No parameters."""
        self.__state = GenState()

    @property
    def state(self) -> None:
        """Get generator state object."""
        return self.__state

    def run(self) -> None:
        """Generate full documentation."""
        self.state.generate()

    def stop(self) -> None:
        """Stop (or cancel) generator."""
        self.state.cancel()


class GenState(StateMachine):
    """Generator state machine."""

    def __init__(self) -> None:
        """No parameters."""
        self.__dry = False
        super().__init__()

    def __log_on_state(self) -> str:
        logger.debug('generator state: {state}'.format(state=self.current))

    def __log_on_transition(self, event) -> str:
        logger.debug('generator transition: {event}'.format(event=event))

    @property
    def current(self) -> str:
        """Get current_state.id."""
        return self.current_state.id

    @property
    def dry(self) -> bool:
        """Get dry state."""
        return self.__dry

    @dry.setter
    def dry(self, b: bool) -> None:
        """Set dry state."""
        if type(b) is bool:
            self.__dry = b
        else:
            raise AttributeError('dry must True or False')

    """
    States
    ------
    10 instanced
    20 TODO initialized
    30 TODO cloned
    40 TODO diagram_maked
    50 TODO attachment_maked
    60 TODO slide_maked
    60 generated
    70 finished
    80 canceled
    90 cleaned
    """
    instanced = State(value=10, initial=True)
    generated = State(value=60)
    finished  = State(value=70)
    canceled  = State(value=80)
    cleaned   = State(value=90, final=True)

    def on_enter_instanced(self):
        """State is now instanced."""
        self.__log_on_state()
        if self.dry:
            return
        else:
            pass

    def on_enter_generated(self):
        """State is now generated."""
        self.__log_on_state()

    def on_enter_finished(self):
        """State is now finished."""
        self.__log_on_state()

    def on_enter_canceled(self):
        """State is now canceled."""
        self.__log_on_state()

    def on_enter_cleaned(self):
        """State is now cleaned."""
        self.__log_on_state()

    """
    Cycles
    ------
    make = cloned.to(diagrams) | diagrams.to(attachments) | attachments.to(slides)
    make()  # (switches only) state to diagrams
    make()  # (switches only) state to attachments
    make()  # (switches only) state to slide

    Transitions
    -----------
    TODO init             = instanced.to(initialized)
    TODO clone            = initialized.to(cloned)
    TODO make_diagrams    = cloned.to(diagrams)
    TODO make_attachments = diagrams.to(attachments)
    TODO make_slides      = attachments.to(slides)
    generate         = slides.to(generated)
    finish           = generated.to(finished)
    cancel           = canceled.from_(
        initialized, cloned, diagrams, attachments, slides, generated
    )  # not finished
    clean            = cleaned.from_(canceled, finished)
    """
    generate = instanced.to(generated)
    finish   = generated.to(finished)
    cancel   = canceled.from_(
        instanced,
        generated,
    )
    clean    = cleaned.from_(finished, canceled)

    def on_generate(self, event):
        """Transition generate."""
        self.__log_on_transition(event)

    def on_finish(self, event):
        """Transition finish."""
        self.__log_on_transition(event)

    def on_cancel(self, event):
        """Transition cancel."""
        self.__log_on_transition(event)

    def on_clean(self, event):
        """Transition clean."""
        self.__log_on_transition(event)
