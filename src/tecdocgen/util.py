"""
TecDocGen utilities module.

Methods
-------
main
    If run as a standalone script.
tmpdir
    Create a temporary directory.

Classes
-------
Antora
    Based on Util.
Asciidoctor
    Based on Util.
AsciidoctorDiagram
    Based on Util.
AsciidoctorPDF
    Based on Util.
Main
    Run module as standalone script.
Util
    Base class.
"""

import errno
import inspect
import logging
import os
import re
import shutil
import subprocess
import tempfile
from contextlib import contextmanager
from importlib.metadata import version
from pathlib import Path

logger = logging.getLogger(__name__)


@contextmanager
def tmpdir() -> None:
    """
    Create a temporary directory.

    Usage
    -----
    with tmpdir() as tmp:
        # store files in tmp
    # the whole directory will be gone when the 'with' statement ends
    """
    __d = tempfile.mkdtemp(prefix=__name__ + '.')
    try:
        yield __d
        logger.debug('mkdtemp {dir}'.format(dir=__d))
    finally:
        shutil.rmtree(__d)


class Util():
    """
    Utility base class.

    Attributes
    ----------
    command : list
    executable : bool
    pushd : str
        Directory path to stack.
    version : str
    """

    def __init__(self) -> None:
        """No parameters."""
        self._command    = []
        self._cmd_opt    = []
        self._cmd_arg    = []
        self._executable = self.__find()
        self._dir_pushd  = None
        self._file       = None
        self._version    = self.__version()

    def __find(self) -> bool:
        self._command = [self._cmd[0]]
        try:
            devnull = open(os.devnull)
            subprocess.Popen(self.command, stdout=devnull, stderr=devnull).communicate()
        except OSError as e:
            if e.errno == errno.ENOENT:
                return False
        else:
            devnull.close()
            return True

    def __version(self):
        self._command = [self._cmd[0], '--version']
        if self.executable:
            try:
                __completed_process = subprocess.run(
                    self.command,
                    capture_output=True,
                    text=True,
                )
                # https://stackoverflow.com/questions/26480935/python-regex-to-extract-version-from-a-string/73329081#73329081
                __versions = re.findall(r'[0-9]+\.[0-9]+\.?[0-9]*', __completed_process.stdout)
                if len(__versions):
                    return __versions[0]
            except subprocess.CalledProcessError as e:
                raise RuntimeError(e)
        else:
            return None

    def _build_command(self) -> None:
        if self._file:
            self._cmd_arg = [self._file]
        else:
            raise RuntimeError('no file given')
        # assemble command parts
        self._command = self._cmd + self._cmd_opt + self._cmd_arg
        logger.debug(
            '{name} # {vars} #'.format(name=__name__, vars=vars(self))
        )

    @property
    def command(self) -> list:
        """Get command list used in 'run'."""
        return self._command

    @property
    def executable(self) -> bool:
        """Get true if 'command' executable."""
        return self._executable

    @property
    def pushd(self) -> str:
        """Get directory path to stack."""
        return self._dir_pushd

    @pushd.setter
    def pushd(self, p: str) -> None:
        """Set directuoy path to stack."""
        self._dir_pushd = p

    @property
    def version(self) -> str:
        """Get utility version."""
        return self._version

    def _run(self) -> None:
        self._build_command()
        if self.executable:
            try:
                self._completed_process = subprocess.run(
                    self.command,
                    cwd=self.pushd,
                    capture_output=True,
                    text=True,
                )
            except subprocess.CalledProcessError as e:
                raise RuntimeError(e)
        else:
            raise RuntimeError(self.cmd[0], 'not executable')

    def run(self) -> None:
        """Run command."""
        self._run()
        if self._completed_process.stderr:
            print(self._completed_process.stderr)
        if self._completed_process.stdout:
            print(self._completed_process.stdout)


class Antora(Util):
    """Antora utility object (child class)."""

    def __init__(self) -> None:
        """No parameters."""
        self._cmd = ['antora']
        super().__init__()
        self._cmd_opt = [
            '--log-format=pretty',
            '--log-level=all',
            '--stacktrace',
        ]

    @property
    def playbook(self) -> str:
        """Get Antora playbook file name."""
        return self._file

    @playbook.setter
    def playbook(self, f: str) -> None:
        """
        Set Antora playbook file name.

        Set playbook path to stack readable from 'pushd'.
        """
        [self._dir_pushd, self._file] = os.path.split(f)
        # TODO
        # self._dir_pushd = '.' if is None

    @property
    def pushd(self) -> str:
        """Get directory path to stack (read only)."""
        return self._dir_pushd


class Asciidoctor(Util):
    """Asciidoctor utility object (child class)."""

    def __init__(self) -> None:
        """No parameters."""
        self._cmd = ['asciidoctor']
        super().__init__()

    @property
    def file(self) -> str:
        """Get asciidoc input file path."""
        return self._file

    @file.setter
    def file(self, f: str) -> None:
        """Set asciidoc input file path."""
        self._file = f


class AsciidoctorDiagram(Util):
    """AsciidoctorDiagram utility object (child class)."""

    def __init__(self) -> None:
        """No parameters."""
        self._cmd = ['asciidoctor', '-r', 'asciidoctor-diagram']
        super().__init__()
        self._cmd_opt = [
            '-D',
            # before run tmpdir path must append here
        ]

    @property
    def file(self) -> str:
        """Get asciidoc input file name."""
        return self._file

    @file.setter
    def file(self, f: str) -> None:
        """
        Set asciidoc input file name.

        Set file path to stack readable from 'pushd'.
        """
        [self._dir_pushd, self._file] = os.path.split(f)

    @property
    def pushd(self) -> str:
        """Get directory path to stack (read only)."""
        return self._dir_pushd

    def run(self) -> None:
        """Run AsciidoctorDiagram.command."""
        with tmpdir() as __tmpd:
            self._cmd_opt.append(__tmpd)
            super().run()


class AsciidoctorPDF(Util):
    """AsciidoctorPDF utility object (child class)."""

    def __init__(self) -> None:
        """No parameters."""
        self._cmd = ['asciidoctor-pdf']
        super().__init__()
        self._cmd_opt = [
            '-o',
            # before run PDF file path/name must append here
        ]
        self._pdf = None

    @property
    def file(self) -> str:
        """Get asciidoc input file name."""
        return self._file

    @file.setter
    def file(self, f: str) -> None:
        """Set asciidoc input file name."""
        self._file = f

    @property
    def pdf(self) -> str:
        """Get the PDF output file name."""
        return self._pdf

    @pdf.setter
    def pdf(self, f: str) -> None:
        """Set the PDF output file name."""
        self._pdf = f

    @property
    def pushd(self) -> str:
        """Overwrite Util.pushd."""
        return self._dir_pushd

    def run(self) -> None:
        """Run AsciidoctorPDF.command."""
        if self.pdf:
            self._cmd_opt.append(self.pdf)
            super().run()
        else:
            raise RuntimeError('no PDF file given')


class Main():
    """Main class."""

    EPILOG = """examples:
    %(prog)s asciidoctor README.adoc
        # Result: README.html

    %(prog)s asciidoctor-diagram docs/modules/ROOT/_diagrams/graphviz.adoc
        # Result: docs/modules/ROOT/images/diagram/graphviz.svg

    %(prog)s asciidoctor-pdf docs/modules/ROOT/pages/development.adoc docs/modules/ROOT/attachments/development.pdf
        # Result: docs/modules/ROOT/attachments/development.pdf

    %(prog)s antora docs/preview.yml
        # Result: docs/_preview/
    """

    def __init__(self) -> None:
        """No parameters."""
        global argparse
        import argparse

        self._args   = None
        self._parser = None
        self.__parse_args()

    def __parse_args(self):
        # create the top-level parser
        self._parser = argparse.ArgumentParser(
            prog='tecdocgen-util',
            formatter_class=argparse.RawTextHelpFormatter,
            epilog=self.EPILOG,
        )
        self._parser.add_argument(
            '--verbose',
            help='increase output verbosity',
            action='store_true',
        )
        self._parser.add_argument(
            '--version',
            help="show version number and exit",
            action='version',
            version='%(prog)s {version}'.format(version=version('tecdocgen')),
        )
        # create the subparser
        __subparsers = self._parser.add_subparsers(
            title='subcommands',
            help='subcommand [-h|--help]',
            dest='sub_cmd',
        )
        # create the parser for a subcommand
        self._parser_asciidoctor = __subparsers.add_parser('asciidoctor')
        self._parser_asciidoctor.add_argument(
            '--version',
            help="show util's version number and exit",
            action='version',
            version='asciidoctor {version}'.format(version=Asciidoctor().version),
        )
        self._parser_asciidoctor.add_argument(
            'FILE',
            help='AsciiDoc input FILE',
        )
        self._parser_asciidoctor.set_defaults(func=self.__asciidoctor)
        # create the parser for a subcommand
        self._parser_asciidoctor_diagram = __subparsers.add_parser('asciidoctor-diagram')
        self._parser_asciidoctor_diagram.add_argument(
            '--all',
            help="run for all files in path",
            action='store_true',
        )
        self._parser_asciidoctor_diagram.add_argument(
            '--version',
            help="show util's version number and exit",
            action='version',
            version='asciidoctor-diagram {version}'.format(version=AsciidoctorDiagram().version),
        )
        self._parser_asciidoctor_diagram.add_argument(
            'FILE',
            help='AsciiDoc input FILE',
        )
        self._parser_asciidoctor_diagram.set_defaults(func=self.__asciidoctor_diagram)
        # create the parser for a subcommand
        self._parser_asciidoctor_pdf = __subparsers.add_parser('asciidoctor-pdf')
        self._parser_asciidoctor_pdf.add_argument(
            '--version',
            help="show util's version number and exit",
            action='version',
            version='asciidoctor-pdf {version}'.format(version=AsciidoctorPDF().version),
        )
        self._parser_asciidoctor_pdf.add_argument(
            'FILE',
            help='AsciiDoc input FILE',
        )
        self._parser_asciidoctor_pdf.add_argument(
            'PDF',
            help='PDF output FILE',
        )
        self._parser_asciidoctor_pdf.set_defaults(func=self.__asciidoctor_pdf)
        # create the parser for a subcommand
        self._parser_antora = __subparsers.add_parser('antora')
        self._parser_antora.add_argument(
            '--version',
            help="show util's version number and exit",
            action='version',
            version='antora {version}'.format(version=Antora().version),
        )
        self._parser_antora.add_argument(
            'PLAYBOOK',
            help='antora playbook file',
        )
        self._parser_antora.set_defaults(func=self.__antora)
        # parse the args
        self._args = self._parser.parse_args()
        logger.debug('parser # {parser} #'.format(parser=self._parser))
        logger.debug('args # {args} #'.format(args=self._args))

    def __antora(self, f: str) -> None:
        util = Antora()
        util.playbook = f
        print('antora', util.playbook)
        return util.run()

    def __asciidoctor(self, f: str) -> None:
        util = Asciidoctor()
        util.file = f
        print('asciidoctor', util.file)
        return util.run()

    def __asciidoctor_diagram(self, f: str) -> None:
        if self._args.all:
            self.__asciidoctor_diagram_all(f)
        else:
            util = AsciidoctorDiagram()
            util.file = f
            print('asciidoctor-diagram', util.file)
            return util.run()

    def __asciidoctor_diagram_all(self, f: str) -> None:
        if self._args.all:
            self._args.all = False
            __dir = f
        if os.path.isfile(__dir):
            [__dir, __file] = os.path.split(__dir)
        if os.path.isdir(__dir):
            print('asciidoctor-diagram', __dir, '--all')
            for __f in Path(__dir).iterdir():
                # print(__diagram)
                self.__asciidoctor_diagram(__f)

    def __asciidoctor_pdf(self, f: str, p: str) -> None:
        util = AsciidoctorPDF()
        util.file = f
        util.pdf  = p
        print('asciidoctor-pdf', util.file, util.pdf)
        return util.run()

    def run(self) -> None:
        """Run the function defined in argument defaults."""
        if self._args.sub_cmd == 'antora':
            self._args.func(self._args.PLAYBOOK)
        elif self._args.sub_cmd == 'asciidoctor':
            self._args.func(self._args.FILE)
        elif self._args.sub_cmd == 'asciidoctor-diagram':
            self._args.func(self._args.FILE)
        elif self._args.sub_cmd == 'asciidoctor-pdf':
            self._args.func(self._args.FILE, self._args.PDF)
        else:
            self._parser.print_help()


def main():
    """Entry point from pyproject.toml."""
    Main().run()


if __name__ == '__main__':
    """Run as a standalone script."""
    import sys

    sys.exit(main())
