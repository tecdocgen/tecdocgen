"""TecDocGen asciidoctor command."""

import click


@click.group(invoke_without_command=True)
@click.pass_context
def asciidoctor(ctx):
    """TODO command usage."""
    if ctx.invoked_subcommand is None:
        # click.echo('I was invoked without subcommand')
        pass
    else:
        # click.echo(f"I am about to invoke {ctx.invoked_subcommand}")
        pass


@asciidoctor.command()
@click.pass_context
def diagram(ctx):
    """TODO subcommand usage."""
    click.echo('The subcommand')


@asciidoctor.command()
@click.pass_context
def pdf(ctx):
    """TODO subcommand usage."""
    click.echo('The subcommand')
