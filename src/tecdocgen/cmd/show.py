"""TecDocGen show command."""

import click
from tecdocgen import tecdoc

# TODO use config option
_file_conf = 'docs/tecdocgen.yml'


@click.group(invoke_without_command=False)
@click.pass_context
def show(ctx):
    """TODO command usage."""
    if ctx.invoked_subcommand is None:
        # click.echo('I was invoked without subcommand')
        pass
    else:
        # click.echo(f"I am about to invoke {ctx.invoked_subcommand}")
        pass


@show.command()
@click.option(
    '--file',
    '__f',
    is_flag=True,
    help='Show config file name only.',
)
@click.pass_context
def config(ctx, __f):
    """Show config file content."""
    __config = tecdoc.TecDocConfig(_file_conf)

    if __f:
        click.echo(__config.file)
    else:
        click.echo(__config.all)
