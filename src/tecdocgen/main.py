"""
TecDocGen main module.

Run TecDocGen as a CLI application.
"""

import click
import os
from importlib import metadata
from tecdocgen.cmd import antora
from tecdocgen.cmd import asciidoctor
from tecdocgen.cmd import show

[__dir, PROG] = os.path.split(os.path.dirname(__file__))


class TecDocGen():
    """Create instance of the tecdocgen CLI."""

    @click.version_option(prog_name=PROG, version=metadata.version(PROG))
    @click.group()
    def cmd():
        """Global usage text here."""
        pass

    """Register all commands."""
    cmd.add_command(antora.antora)
    cmd.add_command(asciidoctor.asciidoctor)
    cmd.add_command(show.show)


def main():
    """Entry point from pyproject.toml."""
    TecDocGen.cmd()
