"""
TecDocGen tecdoc module.

Classes
-------
TecDoc
TecDocConfig
"""

import logging
import yaml
from tecdocgen import docgen

logger = logging.getLogger(__name__)


class TecDoc():
    """
    Technical Documentation object.

    Attributes
    ----------
    config : object

    Methods
    -------
    generate
    """

    def __init__(self, f: str) -> None:
        """
        Initialize the class with the TecDocGen config file path.

        Parameters
        ----------
        f : str
            TecDocGen config file path.
        """
        self.__config = TecDocConfig(f)
        self.__gen = docgen.Generator()

    @property
    def config(self) -> object:
        """Get config object."""
        return self.__config

    def generate(self):
        """Generate full documentation."""
        self.__gen.run()


class TecDocConfig():
    """
    TecDoc config object.

    Attributes
    ----------
    antora : dict
    show : dict
        Show { 'antora': {**}, 'tecdocgen': {**}}.
    tecdocgen : dict
    """

    def __init__(self, f: str) -> None:
        """
        Initialize the class with the TecDocGen config file path.

        Parameters
        ----------
        f : str
            TecDocGen config file path.
        """
        self._path = f

    def __conf(self) -> object:
        """Read YAML file content."""
        try:
            with open(self._path, 'r') as __f:
                __yml = yaml.safe_load(__f)
                # print('DEBUG #', __yml)
                return __yml
        except FileNotFoundError as e:
            print('ERROR #', e)
            return {}

    @property
    def _conf(self) -> object:
        """Get file content object."""
        return self.__conf()

    @property
    def file(self) -> str:
        """Get config file name."""
        return self._path

    @property
    def antora(self) -> dict:
        """Get antora config."""
        if type(self._conf['antora']) is dict:
            return self._conf['antora']
        else:
            return {}

    @property
    def tecdocgen(self) -> dict:
        """Get tecdocgen config."""
        if type(self._conf['tecdocgen']) is dict:
            return self._conf['tecdocgen']
        else:
            return {}

    @property
    def all(self) -> dict:
        """Get config."""
        return {
            'antora': self.antora,
            'tecdocgen': self.tecdocgen,
        }
