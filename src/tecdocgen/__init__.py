"""TecDocGen the Technical Documentation Generator."""

import logging
import os

__all__ = ('__version__', 'tecdocgen')


"""Set package version from file tecdocgen/VERSION."""
with open(os.path.join(os.path.dirname(__file__), 'VERSION')) as __f:
    __version__ = __f.read().strip()


"""
Logging

https://realpython.com/python-logging-source-code/
"""
logging.getLogger(__name__).addHandler(logging.NullHandler())
